%% SVR Nifti to Dicom Writer

sp = 'C:\Users\tr17\Documents\Projects\Misc\2019_09_SVR_NII_to_DICOM_phantom';
cd(sp);

dcmDir = 'dcm';
niiDir = 'nii';

mprDir = '201909131057_PHILIPSECA816E_504_MPR_dcm_test';
svrDir = 'SVR';


%% Load dicoms

% MPR
cd(fullfile(sp, dcmDir, mprDir));
dcmFiles = dir('*.dcm');
MPR.img  = dicomread(dcmFiles.name);
MPR.info = dicominfo(dcmFiles.name);

DCM = MPR;


%% Make fake SVR data
nii.img = rand(256,256,100);
nii.img([1:25,256-25:end],:,:) = 0;
nii.img(:,[1:25,256-25:end],:) = 0;
nii.img(:,:,[1:10,100-10:end]) = 0;


% %% Load niftis
% 
% % SVR
% cd(fullfile(sp, niiDir, svrDir));
% dcmFiles = dir('*.nii.gz');
% nii = load_nii('outputSVRvolume_FINAL.nii.gz');


%% Copy nifti image data to MPR dicom.img

% make datatype same as original dicom
getType = class(DCM.img);
DCM.img = eval([getType '(nii.img)']);


%% Edit dicom header
% DCM.info.StudyDescription = 'RESEARCH - SVR 3D Reconstruction';
DCM.info.SeriesDescription = 'RESEARCH - SVR 3D Reconstruction';


%% Save new dicom
cd(sp);
dicomwrite( reshape(nii.img,[size(DCM.img,1), size(DCM.img,2), 1, size(DCM.img,3)]), ...
    'nii2dcm_b1.dcm', DCM.info, 'CreateMode', 'Copy', 'MultiframeSingleFile', true );




