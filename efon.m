%% Quick script to enter folder open nifti
% - mainly used for saving time when opening .nii folders from Philips
% ISDPACS structure

function [IM, hdr] = efon(folder, singframe)

%%% single frame check
% if singframe = 1, then the script anticipates that the data from ISDPACS
% has been converted from SingleFrame DICOMs. This means the nifti folders
% contain .nii files in the format s*.nii.
if nargin == 1
    singframe = 0;
end

cd(folder);
if singframe == 1
    % single frame open
    niiName = dir('s*.nii');
    nii = load_untouch_nii(niiName.name);

else
    % multiframe open
    nii = load_untouch_nii([folder '.nii.gz']);
end

hdr = nii.hdr;
IM = nii.img;
cd ..

%%% I HAVE EDITED THIS SCRIPT
%%% JOSH IS STILL THE BEST

% fn end
end