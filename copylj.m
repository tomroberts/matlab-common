function [] = copylj()
%% Laurence Jackson, BME, KCL, 2017
% Copy to clipboard without UI elements

%% Main
set(0,'showhiddenhandles','on')
print -noui -clipboard -dbitmap

end