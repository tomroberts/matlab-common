function newnii = applyLoadNiiHdr( loadnii , loaduntouchnii )
%% apply load_nii permutation / rotations

% Allow compatibility with 4D datasets. Don't want to apply any
% permutation/flips if 4D, so just set rot_orient = 4 and apply 0 flip.
if numel(size(loadnii.img)) > 3
    loadnii.hdr.hist.rot_orient(4) = 4;
    loadnii.hdr.hist.flip_orient(4) = 0;
end


% Code lifted/interpreted from xform_nii.m

% Apply load_nii rot_orient to untouched_nii
newnii.img = permute( loaduntouchnii.img , loadnii.hdr.hist.rot_orient );

% Apply load_nii flip_orient to untouched_nii
for i = 1:3
     if loadnii.hdr.hist.flip_orient(i)
           newnii.img = flipdim( newnii.img, i );
     end
end


end