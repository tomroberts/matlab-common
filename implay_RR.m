function handle = implay_RR( DATA, colmap, limits)
%Coloured and scaled implay: limits = [min max], colmap = {jet, gray..etc}

DATA = DATA;

handle = implay(DATA);

if nargin == 1
    colmap = 'gray';
    limits(1) = min(min(min(DATA)));
    limits(2) = 0.75* max(max(max(DATA)));
elseif nargin == 2
    limits(1) = min(min(min(DATA)));
    limits(2) = 0.75* max(max(max(DATA)));
end

handle.Visual.ColorMap.UserRangeMin = limits(1);
handle.Visual.ColorMap.UserRangeMax = limits(2);
handle.Visual.ColorMap.UserRange = 1;

c = [colmap '(256)'];
handle.Visual.ColorMap.MapExpression = c;

set(findall(0,'tag','spcui_scope_framework'),'position',[50 150 800 900]);

% auto maximise
set(0,'showHiddenHandles','on')
fig_handle = gcf ;  
fig_handle.findobj; % to view all the linked objects with the vision.VideoPlayer
ftw = fig_handle.findobj ('TooltipString', 'Maintain fit to window');   % this will search the object in the figure which has the respective 'TooltipString' parameter.
ftw.ClickedCallback();  % execute the callback linked with this object

end

