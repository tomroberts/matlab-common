function figurefonts()

% quick function so don't have to keep change figure fonts

set(gca,'FontSize',16,'FontName','Lato');
% set(gca,'FontWeight','Bold');

% Blank background
set(gcf,'Color','White'); % set = None to make transparent
set(gca,'Color','None');


% end fn
end