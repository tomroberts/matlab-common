function fdf2mat2(inPath, outPath, matName)

% Johannes Riegler <j.riegler@ucl.ac.uk>, UCL CABI 15-06-2008

% This file converts fdf cine images into 4D stack and saves it as
% "inPath.mat"

% This makro is handling two options at the moment:
% Option 1: several slices
%
% The inPath folder should contain all the slices which shall be included.
% It is assumed that the only change in the short axis slices is in the
% relative z direction, meaning that the slices build a cube.
%
% Copy all *.img folders for the slices in the inPath folder. The folders should be named in such a way
% that alpahbetic ordering arranges them in increasing slice numbers. 
% 
% e.g.
% cine_SA01.img
% cine_SA02.img
% cine_SA03.img
%
%
% Do not delete the prop file in the *.img folder since file postions for start are hard coded.
% You can also add long-axis views aslong as the Matrix size, number of frames and resolution
% is the same as for short axis-views.
% The long-axis views will appear before or after the short-axis stack.
%
% Option 2: multislice
% The inPath folder should contain all the fdf fiels and the prop file. The
% makro should detect that there are no slices and change the conversion
% accordingly.


folders = dir(inPath);
folder1 = [inPath '/' folders(3).name];
filesInFolder = dir(folder1);
fIF = size(filesInFolder);
nFiles = fIF(1)-3;
Subfolders = 1;

% check if folder contains subfolders in not number of size in the folder
% otherwise look into subfolders

if nFiles < 1,
    filesInFolder = dir(inPath);
    fIF = size(filesInFolder);
    nFiles = fIF(1)-3;
    Subfolders = 0;
    file1 = [inPath '/' filesInFolder(4).name];
    
else  
    file1 = [folder1 '/' filesInFolder(4).name];
    subFolders = size(folders)-2;
    numberOfSlices = subFolders(1);
    fileCheck = 1;
    Subfolders = 1;
end

% skip subfolder check if there are no subfodlers

if Subfolders == 1,
    % check if number of files in the folders are equal
    for a = 2:numberOfSlices
        folderx = [inPath '/' folders(2+a).name];
        filesInFolderx = dir(folderx);
        fIFx = size(filesInFolderx);
        nFilesx = fIFx(1)-3;
    
        if nFilesx == nFiles,
            fileCheck = fileCheck + 1;  
        else     
        end
    end
      if numberOfSlices == fileCheck,
         disp('The number of files in subfolders is consistent.')    
      else
         msgbox('The number of files in each subfolder must be equal!' ,'Error message','error')
      end
else
end    
    
% ask for heart rate and scale up factor
prompt={'Enter heart rate [bpm]:', 'Enter scale up factor:'};
def={'350', '1'}; 
input=inputdlg(prompt, 'Please specify the parameters!', 1, def);

heartRate1 = str2num(cell2mat(input(1)));
scaleFactor = str2num(cell2mat(input(2)));

% load first fdf file to create header for mat file (only necessary
% Structure elements for Segment are specified
 
[hdr img] = fdfread2(file1, scaleFactor);

FileModDate = filesInFolder(4).date;
Res = (hdr.PixelSpacing);
Rows1 = hdr.Rows;
Columns1= hdr.Columns;

% reset parameters for folder use
if Subfolders == 0,
   numberOfSlices = nFiles / hdr.CardiacNumberOfImages;
   nFiles = hdr.CardiacNumberOfImages;
else
end    

% header file
info.Modality=hdr.Modality;
info.Manufacturer=hdr.Manufacturer;
info.InstitutionName=hdr.InstitutionName;
info.Rows=hdr.Rows;
info.Columns=hdr.Columns;
info.AcquisitionMatrix=hdr.AcquisitionMatrix;
info.PixelSpacing=hdr.PixelSpacing;
info.ImagedNucleus=hdr.ImagedNucleus;
info.ImagingFrequency=hdr.ImagingFrequency;
info.TE=hdr.te;
info.TR=hdr.RepetitionTime;
info.SequenceName=hdr.SequenceName;
info.StudyDescription=hdr.StudyDescription;
info.ImagePositionPatient=hdr.ImagePositionPatient;
info.ImageOrientationPatient=hdr.ImageOrientationPatient;
info.MRAcquisitionType=hdr.MRAcquisitionType;
info.MagneticFieldStrength=hdr.MagneticFieldStrength;
info.Name=inPath;
info.ID=hdr.StudyID;
info.BirthDate='not given';
info.Sex='not given';
info.Age='not given';
info.AcquisitionDate=FileModDate; 
info.Length='not given';
info.Weight='not given';
info.BSA='not given';
info.NFrames=hdr.CardiacNumberOfImages;
info.NumSlices=numberOfSlices;
info.slices=hdr.slices;
info.Resolution=Res(1);
info.HeartRate=heartRate1;
info.SliceThickness=(hdr.SliceThickness); 
info.SliceGap=hdr.SpacingBetweenSlices;
info.TIncr=(hdr.RepetitionTime)*0.001; 
info.ImageType='MRGE';



% start building up 4D array


    for z = 1:numberOfSlices
        
        % change folder if Subfolder is 0
        
            if Subfolders == 1,
            folderx = [inPath '/' folders(numberOfSlices+3-z).name];
            else
            folderx = inPath;
            end
        
            
            
        for t = 1:nFiles 
            
            if Subfolders == 1,
            filex = [folderx '/' filesInFolder(3+t).name];
            else
            n = (z-1)*hdr.CardiacNumberOfImages + t;    
            filex = [folderx '/' filesInFolder(3+n).name];
            end
                       
            [hdr img] = fdfread2(filex, scaleFactor);
            
            % create vector with all TI and te values
                i=(z-1)*hdr.CardiacNumberOfImages + t;
                info.TI(i)=hdr.TI;
                info.te(i)=hdr.te*1000;
                            
            if (z==1) && (t==1),
              im=img;
            else
              
              % check for agreement in matrix size  
              if (Rows1 == hdr.Rows) && (Columns1 == hdr.Columns),  
              im(:,:,t,z)=img;
              else
              msgbox('The matrix size of images in subfolders must agree' ,'Error message','error')    
              end
              
            end
        
        end
        
    end
      
    
% save output together with header
InputString = strfind(inPath, '/');

if InputString >= 1,  
   outputFile=[outPath '/' matName '.mat'];
    
else  
   outputFile=[outPath '/' matName '.mat']; %inPath '.mat']; -Edited out by Tomo
end  

% for LGE data, just do as normal --- TAR
if strcmp(info.SequenceName,'IRcine') == 1
    save(outputFile, 'info', 'im');
    disp(['The conversion of the fdf files contained in folder ' inPath ' was successful.'])
    
    
% for T2 data in form of multislice data, create separate outputfiles for
% each slic
    
elseif info.slices > 1,
   nEchos=info.NumSlices/info.slices;
   
   for n=1:info.slices
       low=(n*nEchos)-nEchos+1;
       high=n*nEchos;     
       imx=im(:,:,:,low:high);
      % tex=info.te(low:high); 
       tex=info.te(1:nFiles); 
       infox=info;
       infox.te=tex;
       nAsString=num2str(n);
       
       if InputString >= 1,
          outputFilex=[outPath '/' matName nAsString '.mat'];
          else  
          outputFilex=[outPath '/' inPath nAsString '.mat'];
       end
       
       save(outputFilex, 'infox', 'imx');
       disp(['The conversion of slice number ' nAsString ' of the fdf files contained in folder ' inPath ' was successful.'])
       
   end
   
% normal case: one outputfile (e.g. cardiac data)   
else
save(outputFile, 'info', 'im');
disp(['The conversion of the fdf files contained in folder ' inPath ' was successful.'])    
    
end    
    



end

