%% SVR Nifti to Dicom Writer

cd('C:\Users\tr17\Documents\Projects\Misc\2019_07_SVR_to_DICOM_recons_for_Mary\P1\SVR_recon');

% load existing dicom to use as base
D.img = dicomread('C:\Users\tr17\Documents\Projects\Misc\2019_07_SVR_to_DICOM_recons_for_Mary\P1\AA2EBA1F\AA62DF39\0000A103\EE0AA165');
D.info = dicominfo('C:\Users\tr17\Documents\Projects\Misc\2019_07_SVR_to_DICOM_recons_for_Mary\P1\AA2EBA1F\AA62DF39\0000A103\EE0AA165');

nii = load_nii('outputSVRvolume_FINAL.nii.gz');


% edit metadata
D.info.ImagePositionPatient = zeros(size(D.info.ImagePositionPatient));
D.info.ImageOrientationPatient = zeros(size(D.info.ImageOrientationPatient));

D.info.SeriesDescription = 'RESEARCH 3D Reconstruction';

%% multiframe
mkdir dcm_mf
cd dcm_mf

dicomwrite( reshape(nii.img,[size(nii.img,1), size(nii.img,2) 1 size(nii.img,3)]), ...
    'SVR_recon_3D.dcm', D.info, 'CreateMode', 'copy', 'MultiframeSingleFile', false );

dicomanon('SVR_recon_3D.dcm','SVR_recon_3D.dcm','keep',{'AccessionNumber','SeriesDescription'});

Dnew.info = dicominfo('SVR_recon_3D_001.dcm');

%% singleframe
mkdir dcm_sf
cd dcm_sf

D.info.MRAcquisitionType = '3D';

dicomwrite( reshape(nii.img,[size(nii.img,1), size(nii.img,2) 1 size(nii.img,3)]), ...
    'SVR_recon_3D.dcm', D.info, 'CreateMode', 'copy', 'MultiframeSingleFile', false );

for ii = 1:size(nii.img,3)
        dicomanon(['SVR_recon_3D_' sprintf('%03d',ii) '.dcm'], ...
                  ['SVR_recon_3D_' sprintf('%03d',ii) '.dcm'], ...
                   'keep',{'AccessionNumber','SeriesDescription'});
end

D1.info = dicominfo(['SVR_recon_3D_001.dcm']);
D126.info = dicominfo(['SVR_recon_3D_126.dcm']);

cd ..

%%%TODO: think I need to update the headers for each singleframe. Currently
%%%headers are identical and therefore not opening in single Instance.


