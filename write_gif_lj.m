function [] = write_gif_lj(imgif,filename,varargin)
%%


%% remember to add this bit in your for loop
%   plot(XX,YY);
%   drawnow
%   f = getframe;
%   imgif{puls} = frame2im(f);

%% parse
dt = 1;
LoopCount = inf;

for kk = 1:length(varargin)
    
    if strcmpi(varargin{kk},'DelayTime')
        dt = varargin{kk+1};
        continue;
    end
    
    if strcmpi(varargin{kk},'LoopCount')
        LoopCount = varargin{kk+1};
        continue;
    end
    
end

ss = find(cellfun(@isempty,imgif));
if ~isempty(ss)
    imgif(ss) = [];
end
%% write
% filename = 'testAnimated.gif'; % Specify the output file name
nImages = size(imgif,2);

for idx = 1:nImages
    
    [A,map] = rgb2ind(imgif{idx},256);
    if idx == 1
        imwrite(A,map,filename,'gif','LoopCount',LoopCount,'DelayTime',dt);
    else
        imwrite(A,map,filename,'gif','WriteMode','append','DelayTime',dt);
    end
end

end

