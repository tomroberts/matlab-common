function video2gif(videoFile)

v = VideoReader(videoFile);

numFrames = v.NumberOfFrames;
ctr = 1;

% odd workaround - have to recreate v once you access it...
v = VideoReader(videoFile);

disp('Processing video file...')

currAxes = axes;
while hasFrame(v)
    
    % display video frame as MATLAB figure
    vidFrame = readFrame(v);
    AllFrames(:,:,:,ctr) = vidFrame;
    ctr = ctr+1;
    image(vidFrame, 'Parent', currAxes);
    currAxes.Visible = 'off';
    axis image;
    
end

close(gcf);

disp('Finished processing video file.')


%% Make GIFs

disp('Creating GIF...')

figure('units','normalized','outerposition',[0 0 1 1])

for tt = 1:numFrames
    imagesc(AllFrames(:,:,tt)); colormap('gray');
    axis image;
%     truesize([600 600]);
%     set(gcf,'defaultfigurecolor',[1 1 1]); % remove grey background - doesn't seem to work...
    set(gcf,'color','white'); % make background white
%     set(gcf,'color','none'); % makes transparent, but doesn't work for gif
%     set(gca,'color','none');
    set(gca,'visible','off');
    
    % minimise area around axes
    ax = gca;
    outerpos = ax.OuterPosition;
    ti = ax.TightInset; 
    left = outerpos(1) + ti(1);
    bottom = outerpos(2) + ti(2);
    ax_width = outerpos(3) - ti(1) - ti(3);
    ax_height = outerpos(4) - ti(2) - ti(4);
    ax.Position = [left bottom ax_width ax_height];
    
    f = getframe(gcf);
    imgif{tt} = frame2im(f);
    clf;
end
write_gif_lj(imgif,[videoFile(1:end-4) '.gif'],'DelayTime',1/v.FrameRate);
close all

disp('GIF creation complete!')


%fn end
end