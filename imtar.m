function imtar(Image,Cl,Cu,figType)

% imtar
%
% Quick function so I don't keep having to type out a long boring line!
%
% v2.0 of best ever function:
% - 17-04-2019
% - added ability to remove grey area padding in MATLAB figure and remove
% ticks and thin black border. Good for images in papers.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     disp(' I ? Baz');

    % automatically display absolute of image
    if ~isreal(Image)
        Image = abs(Image);
    end

    if nargin == 1
        figure; imagesc(Image(:,:)), colormap(gray), colorbar, axis image;
    elseif nargin > 1 && nargin < 4
        figure, imagesc(Image(:,:),[Cl Cu]), colormap(gray), colorbar, axis image;
    elseif nargin == 4
        if strcmp(figType,'paperFig')
        
            if isempty(Cl) && isempty(Cu)
                figure, imagesc(Image(:,:)), colormap(gray), colorbar, axis image;
            else
                figure, imagesc(Image(:,:),[Cl Cu]), colormap(gray), colorbar, axis image;
            end
                
            % remove borders/ticks
            set(gca,'xticklabel',[]); 
            set(gca,'xtick',[]); 
            set(gca,'yticklabel',[]); 
            set(gca,'ytick',[]); 
            set(gca,'zticklabel',[]); 
            set(gca,'ztick',[]); 
            set(gca,'visible','off'); 
            colorbar('off');
            
            % remove figure grey area
            ax = gca;
            outerpos = ax.OuterPosition;
            ti = ax.TightInset; 
            left = outerpos(1) + ti(1);
            bottom = outerpos(2) + ti(2);
            ax_width = outerpos(3) - ti(1) - ti(3);
            ax_height = outerpos(4) - ti(2) - ti(4);
            ax.Position = [left bottom ax_width ax_height];
            
        end
    end