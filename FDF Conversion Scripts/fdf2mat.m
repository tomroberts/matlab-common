function im = fdf2mat(path, newName)

% 29.07.2008 Johannes Riegler
% This fuction needs a modified version of fdfreadone.
% The images from the specified folder are put to gether as a 3D stack.


files = dir(path);
n = size(files)-3;
scaleFactor=1;

for i=1:n
    filename=[path '/' files(i+3).name];
    [header img] = fdfreadone(filename, scaleFactor);
            if i==1,
                im=img;
            info.TI(i)=header.TI;
            info.te(i)=header.te*1000;
            info.TR=header.RepetitionTime;
            info.ID=header.StudyID;
            else
               im(:,:,i)=img;
                info.TI(i)=header.TI;
                info.te(i)=header.te*1000;

            end
end

info.Name=path;




outputFile=[newName '.mat'];
ni = ['the images have been saved as:' pwd '/' outputFile];
save([outputFile], 'info', 'im');
jack=im;


disp('fdf2mat reconstruction complete.');

end

