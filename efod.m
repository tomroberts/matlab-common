%% Quick script to enter folder open dicom
% - mainly used for saving time when opening .dicom folders from Philips
% ISDPACS structure

function [IM, INFO] = efod(folder, singframe)

%%% single frame check
% if singframe = 1, then the script anticipates that the data from ISDPACS
% has been converted from SingleFrame DICOMs. This means the nifti folders
% contain .nii files in the format s*.nii.
if nargin == 1
    singframe = 0;
end

cd(folder);
if singframe == 1
    % single frame open
    error('NEED TO IMPLEMENT ABILITY TO OPEN SINGLE FRAME DICOMS');
else
    % multiframe open
    dcmName = dir('*.dcm');
    dcm = dicomread(dcmName.name);
    info = dicominfo(dcmName.name);
end

IM = dcm;
INFO = info;
cd ..

%%% I HAVE EDITED THIS SCRIPT
%%% JOSH IS STILL THE BEST
%%% LAURENCE IS THE WORST

% fn end
end