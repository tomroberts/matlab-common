function [header img] = fdfreadone1(filename, scaleFactor)

% [hdr img] = readfdf(filename)
%
% MJ White <mark.white@ucl.ac.uk>, UCL CMIC, 2008-06-26
%
% Reads a single FDF file and returns a reasonably populated DICOM
% header and the image structure itself.
%
% The written dicom files are somewhat fake, and the code may need
% customizing for other types of studies (I wrote it for 2D multi-echo
% cardiac studies).  The DICOM header does not contain all the fields
% officially needed for MR modality.
%
% Distantly based on fdf.m by Shanrong Zhang <zhangs@u.washington.edu>
%
% This function does not require the Image Processing Toolbox, but the
% header struct it returns is intended to match that of dicominfo().

% open file, skip shell line
disp(filename);
fid = fopen(filename, 'r');
line = fgetl(fid);

done = 0;
header = struct();
machineformat = 'ieee-be';

% aux functions for very simple value unpacking
  function str = unquote(s)
    t = regexp(s,'^"(.*)"$','tokens');
    str = cell2mat(t{1,1}(1));
  end

  function res = split2(s)
    t = regexp(s, '{(\S+),\s?(\S+)}', 'tokens');
    res = [(t{1,1}(1)) (t{1,1}(2))];
  end

  function res = split3(s)
    t = regexp(s, '{(\S+),\s?(\S+),\s?(\S+)}', 'tokens');
    res = [(t{1,1}(1)) (t{1,1}(2)) (t{1,1}(3))];
  end

function res = split9(s)
    t = regexp(s,'{(\S+),\s?(\S+),\s?(\S+),\s?(\S+),\s?(\S+),\s?(\S+),\s?(\S+),\s?(\S+),\s?(\S+)}','tokens');
    res = [(t{1,1}(1)) (t{1,1}(2)) (t{1,1}(3)) (t{1,1}(4)) (t{1,1}(5)) (t{1,1}(6)) (t{1,1}(7)) (t{1,1}(8)) (t{1,1}(9))];
end

  function res = cellnum(c)
    N = size(c);
    res = zeros(N);
    for x=1:N(1)
      for y=1:N(2)
        res(x,y) = str2num(cell2mat(c(x,y)));
      end
    end
  end

while (~isempty(line) && ~done)

  line = fgetl(fid);

  % stop on FF character (end of header)
  if size(regexp(line,'^\o14')) > 0

    done = 1;

  else

    t = regexp(line, '^(\w+)\s+(\S+)\s+=\s+(.*);$', 'tokens');
    type = cell2mat(t{1,1}(1));
    id   = cell2mat(t{1,1}(2));
    val  = cell2mat(t{1,1}(3));

    if strcmp(id,'bits')
      bits = str2num(val);
    end

    if strcmp(id,'matrix[]')
      res = split2(val);
      M(1) = str2num(cell2mat(res(1)));
      M(2) = str2num(cell2mat(res(2)));

      header.Height = M(1);
      header.Width = M(2);
      header.Rows = M(1);
      header.Columns = M(2);
      header.NumerOfPhaseEncodingSteps = M(1);
      header.AcquisitionMatrix = [0 M(1) M(2) 0];
    end

    if strcmp(id,'bigendian')
      if str2num(val) == 1
        machineformat = 'ieee-be';
      else
        machineformat = 'ieee-le';
      end
    end

    header.Modality = 'MR';
    header.Manufacturer = 'Varian';
    header.InstitutionName = 'University College London';

    if strcmp(id,'*studyid')
      header.StudyDescription = unquote(val);
      header.StudyID = unquote(val);
    end

    if strcmp(id,'*sequence')
      header.SeriesDescription = unquote(val);
      header.SequenceName = unquote(val);
    end

    if strcmp(id,'TR')
      header.RepetitionTime = str2num(val);
    end

    if strcmp(id,'TI')
      header.TI= str2num(val);
    end

    if strcmp(id,'ti')
      header.TI= str2num(val)*1000;
    end

    if strcmp(id, 'te')
       header.te=str2num(val);
    end

    if strcmp(id,'TE')
      header.EchoTime = str2num(val);
      header.TriggerTime = str2num(val);
      header.ContentTime = val;
      header.AcquisitionTime = val;
    end

    if strcmp(id,'*nucleus[]')
      res = split2(val);
      header.ImagedNucleus = unquote(cell2mat(res(1)));
    end

    if strcmp(id,'nucfreq[]')
      res = split2(val);
      header.ImagingFrequency = str2num(cell2mat(res(1)));
    end

    if strcmp(id,'span[]')
        span = cellnum(split2(val));
    end

    if strcmp(id,'orientation[]')
        orient = cellnum(split9(val));
    end

    if strcmp(id,'phi')
      phi_d = str2num(val);
      header.FlipAngle =phi_d;
    end

    if strcmp(id,'psi')
      psi_d = str2num(val);
    end

    if strcmp(id,'theta')
      theta_d = str2num(val);
    end

    if strcmp(id,'gap')
      gap = str2num(val);
    end


    if strcmp(id,'echoes')
      header.NumberOfTemporalPositions = str2num(val);
      header.CardiacNumberOfImages = str2num(val);
      header.EchoTrainLength = 1;
    end

    if strcmp(id,'echo_no')
      header.EchoNumber = str2num(val);
      header.TemporalPositionIdentifier = str2num(val);
      header.ContentTime = val;
    end

  % % Not currently handling multiple slices in one series
  % if strcmp(id,'slices')
  %   header. = str2num(val);
  % end
  %
  % if strcmp(id,'slice_no')
  %   header. = str2num(val);
  % end

    if strcmp(id,'roi[]')
      roi = cellnum(split3(val));
    end

    if strcmp(id,'location[]')
      loc = cellnum(split3(val));
    end
  end
end

if isnumeric(scaleFactor),
sF=scaleFactor*10;
else
sF=str2num(scaleFactor)*10;
end

   % MutR transform-matrix to convert origin given by user coordinatesystem(xyz) to
   % scanner coordinate system (XYZ), MRtU is the back transformation matrix.
   % TRx180 rotated the whole system by 180 deg. along the x-axis since thevarian fdf files give the output in the wrong orientation.
   % After that, the origin is shifted to the lower left hand corner.


   MutR=[orient(1) orient(4) orient(7); orient(2) orient(5) orient(8);
orient(3) orient(6) orient(9)];
   MRtU=[orient(1) orient(2) orient(3); orient(4) orient(5) orient(6);
orient(7) orient(8) orient(9)];
   originUser=[loc(1); loc(2); loc(3)];
   oriRwrong = MutR*originUser;
   VXwrong=[orient(1); orient(2); orient(3)];
   VYwrong=[orient(4); orient(5); orient(6)];
   VZwrong=[orient(7); orient(8); orient(9)];
   TRx180=[1 0 0; 0 -1 0; 0 0 -1];
   oriR=TRx180*oriRwrong;
   VX=TRx180*VXwrong;
   VY=TRx180*VYwrong;
   VZ=TRx180*VZwrong;
   MRtUnew=[VX(1) VX(2) VX(3); VY(1) VY(2) VY(3); VZ(1) VZ(2) VZ(3)];
   oriInNewReference=MRtUnew*oriR;
   VXneg=VX*-span(1)/2;
   VYneg=VY*-span(2)/2;
   oriNew=VXneg+VYneg+oriR;


header.PixelSpacing = [(roi(1)/M(1))*sF;(roi(2)/M(2))*sF];
header.SliceLocation = 0; % (loc(3))*sF;
header.StageCodeSequence = (loc(3))*sF;
header.SliceThickness = roi(3)*sF;
header.SpacingBetweenSlices = gap*sF;
header.ImagePositionPatient = [oriNew(1)*sF;oriNew(2)*sF;oriNew(3)*sF];
header.ImageOrientationPatient = [VX(1);VX(2);VX(3);VY(1);VY(2);VY(3)];
header.ScanningSequence = 'GE';
header.SequenceVariant = 'NONE';
header.ScanOptions = 'CG';
header.MRAcquisitionType = '2D';
header.Trigger = 1.0;
header.NumberOfAverages = 1.0;
header.MagneticFieldStrength = 9.4;
header.PercentSampling = 100;
header.PercentPhaseFieldOfView = (roi(2)/roi(1));
header.ReconstructionDiameter = roi(1);
header.Format = 'DICOM';

skip = fseek(fid, -M(1)*M(2)*bits/8, 'eof');
img  = fread(fid, [M(1), M(2)], 'float32', machineformat);

img = img';
fclose(fid);


end
