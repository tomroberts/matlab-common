%% Quick script to enter folder open QFlow data
% - mainly used for saving time when opening QFlow folders from Philips
% ISDPACS structure

function [MAG, VEL, DPH, Q, nii_mag, nii_ph] = efoq(folder)

cd(folder);

files = dir; files = files(3:end);

if strfind(files(1).name,'it0') > 0
    nii_mag = load_untouch_nii( '*it0*.nii.gz' );
%     nii_mag = load_nii( '*it0*.nii.gz' );
    Q.voxsiz = nii_mag.hdr.dime.pixdim(2:4);
    MAG = double(nii_mag.img(:,:,1:2:end));
    nii_ph = load_untouch_nii( '*it3*.nii.gz' );
%     nii_ph = load_nii( '*it3*.nii.gz' );
    PH = double(nii_ph.img); %<- scanner values, need converting
    Q.inter = nii_ph.hdr.dime.scl_inter;
    Q.slope = nii_ph.hdr.dime.scl_slope;
    VEL = ( PH .* Q.slope ) + Q.inter;
    DPH  = ( VEL * pi ) ./ abs(Q.inter);
    Q.numPhases = size(MAG,3);
else
    nii_mag = load_untouch_nii( files(1).name ); 
    Q.voxsiz = nii_mag.hdr.dime.pixdim(2:4);
    MAG = double(nii_mag.img(:,:,1:end/2));
    nii_ph = load_untouch_nii( files(2).name ); 
    PH = double(nii_ph.img); %<- scanner values, need converting
    Q.inter = nii_ph.hdr.dime.scl_inter;
    Q.slope = nii_ph.hdr.dime.scl_slope;
    VEL = ( PH .* Q.slope ) + Q.inter;
    DPH  = ( VEL * pi ) ./ abs(Q.inter);
    Q.numPhases = size(MAG,3);
end

cd ..

% fn end
end


