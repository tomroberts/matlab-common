function [imx, info]=fdf2mat2_RR(inPath)
% Edited RR 12/12/2012
if nargin<1
    inPath=uigetdir;
end

%% Finds only fdf image files in folder
folders = dir(inPath);
for i=1:length(folders)
    B=strfind(folders(i).name, '.fdf');
    if isempty(B)==1
        C(i)=0;
%         B=strfind(folders(i).name, 'procpar');
%         if B==1;
%             procparloc=i;
%         end
    else
        C(i)=1;
    end
end
FilesInd=(find(C));
nFiles=length(find(C));

%% Load in Header
[hdr img] = fdfread2([inPath '/' folders(FilesInd(1)).name], 1);

    %% Choose returned info 
% FileModDate = filesInFolder(4).date;
Res = (hdr.PixelSpacing);
% info.Modality=hdr.Modality;
% info.Manufacturer=hdr.Manufacturer;
% info.InstitutionName=hdr.InstitutionName;
info.Rows=hdr.Rows;
info.Columns=hdr.Columns;
info.AcquisitionMatrix=hdr.AcquisitionMatrix;
info.PixelSpacing=hdr.PixelSpacing;
% info.ImagedNucleus=hdr.ImagedNucleus;
info.ImagingFrequency=hdr.ImagingFrequency;
% info.TE=hdr.te;
info.TR=hdr.RepetitionTime;
info.SequenceName=hdr.SequenceName;
info.StudyDescription=hdr.StudyDescription;
% info.ImagePositionPatient=hdr.ImagePositionPatient;
% info.ImageOrientationPatient=hdr.ImageOrientationPatient;
info.MRAcquisitionType=hdr.MRAcquisitionType;
% info.MagneticFieldStrength=hdr.MagneticFieldStrength;
info.Name=inPath;
% info.ID=hdr.StudyID;
% info.BirthDate='not given';
% info.Sex='not given';
% info.Age='not given';
% info.AcquisitionDate=FileModDate;
% info.Length='not given';
% info.Weight='not given';
% info.BSA='not given';
% info.NFrames=hdr.CardiacNumberOfImages;
% info.NumSlices=numberOfSlices;
info.slices=hdr.slices;
info.Resolution=Res(1);
% info.HeartRate=heartRate1;
info.SliceThickness=(hdr.SliceThickness);
info.SliceGap=hdr.SpacingBetweenSlices;
info.TIncr=(hdr.RepetitionTime)*0.001;
% info.ImageType='MRGE';

%% Load 3D array
for m=1:nFiles
    filex = [inPath '/' folders(FilesInd(m)).name];
    [hdr img(:,:,m)] = fdfread2(filex, 1);
end
nEchos=nFiles/info.slices; 
imx=reshape(img, info.Rows,info.Columns,nEchos,info.slices);

%% Create TE
TEstart=FilesInd(1:nEchos);
for m=1:nEchos;
    filex = [inPath '/' folders(TEstart(m)).name];
    [hdr] = fdfread2(filex, 1);
    info.te(m)=hdr.te*1000; % ms
end

end

