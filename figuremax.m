function figuremax()

% quick function so don't have to keep writing maximise figure code

figure('units','normalized','outerposition',[0 0 1 1]);

% Blank background
set(gcf,'Color','White'); % set = None to make transparent
set(gca,'Color','None');

% end fn
end